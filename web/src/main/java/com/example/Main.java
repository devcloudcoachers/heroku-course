/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.force.api.ApiVersion;
import com.force.api.ApiConfig;
import com.force.api.ForceApi;
import com.salesforce.emp.connector.BayeuxParameters;
import com.salesforce.emp.connector.EmpConnector;
import static com.salesforce.emp.connector.LoginHelper.login;
import com.fasterxml.jackson.annotation.JsonProperty;

@Controller
@SpringBootApplication
public class Main {

	@Value("${spring.datasource.url}")
	private String dbUrl;

	@Autowired
	private DataSource dataSource;

	private static List<String> receivedEvents = new ArrayList<String>();

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Main.class, args);
		System.out.println("Port: " + System.getenv("PORT"));

		// Initialize CDC
		String user = System.getenv("SF_USER");
		String pwd = System.getenv("SF_PWD");
		if (user!=null && pwd!=null) {
			Consumer<Map<String,Object>> consumer = events -> receivedEvents.add(events.toString());
			BayeuxParameters params = login(user, pwd);
			EmpConnector connector = new EmpConnector(params);
			connector.start().get(5, TimeUnit.SECONDS);
			connector.subscribe("/data/AccountChangeEvent", EmpConnector.REPLAY_FROM_EARLIEST, consumer).get(5,TimeUnit.SECONDS);
		}
	}

	@RequestMapping("/")
	String index() {
		return "index";
	}

	@RequestMapping("/db")
	String db(Map<String, Object> model) {
		try (Connection connection = dataSource.getConnection()) {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS timestps (timestp timestamp, id BIGSERIAL PRIMARY KEY)");
			stmt.executeUpdate("INSERT INTO timestps VALUES (now())");
			ResultSet rs = stmt.executeQuery("SELECT timestp FROM timestps");

			ArrayList<String> output = new ArrayList<String>();
			while (rs.next()) {
				output.add("Read from DB: " + rs.getTimestamp("timestp"));
			}

			model.put("records", output);
			return "db";
		} catch (Exception e) {
			model.put("message", e.getMessage());
			return "error";
		}
	}

	@RequestMapping("/platformevents")
	String platformevents() {
		ApiConfig config = new ApiConfig().setApiVersion(ApiVersion.V39);
		config.setUsername(System.getenv("SF_USER"));
		config.setPassword(System.getenv("SF_PWD"));

		ForceApi api = new ForceApi(config);

		VoteEvent event = new VoteEvent();
		event.value = 9;

		api.createSObject("Vote__e", event);
		return "platformevents";
	}

	@RequestMapping("/cdc")
	String cdc(Map<String, Object> model) {
		model.put("records", receivedEvents);
		return "cdc";
	}

	@Bean
	public DataSource dataSource() throws SQLException {
		if (dbUrl == null || dbUrl.isEmpty()) {
			return new HikariDataSource();
		} else {
			HikariConfig config = new HikariConfig();
			config.setJdbcUrl(dbUrl);
			return new HikariDataSource(config);
		}
	}

	public class VoteEvent {

		@JsonProperty("Value__c")
		Integer value;

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}
	}

}
